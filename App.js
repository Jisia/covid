import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Localisation from './Components/Localisation';
export default function App() {
  return (
   <Localisation />
  );
}

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });
