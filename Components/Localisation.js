import React from 'react';
import { StyleSheet, Text, View, Dimensions,Button,Alert } from 'react-native';
import MapView , { PROVIDER_GOOGLE , Marker}  from 'react-native-maps';
class Localisation extends React.Component {

    constructor(props) {
    super(props);

    this.state = {

      region:{
       latitude: 33.520593,
       longitude: -7.568060,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,
    },
    markers:[],

    initialPosition: 'unknown',
      lastPosition: 'unknown',

      latitude : null,
      longitude : null,

    };
  }

watchID: ?number = null;
   componentDidMount = () => {
      navigator.geolocation.getCurrentPosition(
         (position) => {
            const initialPosition = JSON.stringify(position);
            this.setState({ initialPosition });

         },
         (error) => alert(error.message),
         { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
      );
      this.watchID = navigator.geolocation.watchPosition((position) => {
         const lastPosition = JSON.stringify(position);
         this.setState({ lastPosition });
      });
   }
   componentWillUnmount = () => {
      navigator.geolocation.clearWatch(this.watchID);
   }
   onPressLearnMore(){
navigator.geolocation.getCurrentPosition(
         (position) => {
            const initialPosition = JSON.stringify(position);
            this.setState({ initialPosition });
            alert(this.state.initialPosition);
         },
         (error) => alert(error.message),
         { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
      );
  }
	render(){
		return(
				// <View style={{ marginTop: 20}}>
				// <TextInput placeholder="Titre" />
				// <Button title="Rechercher" onPress={() => {}} />
				// </View>

				 <View style={styles.container}>

       				 <MapView style={styles.mapStyle} 
						     region={this.state.region}                           
       				 >
       		
					     
						    <Marker
						        coordinate={{
									 latitude: 33.520593,
      								 longitude: -7.568060,
									}}
						        icon={require('../assets/index.png')}
						    />
						  
					   </MapView>

				
        				<Button style={{marginTop: 500}} title="Geolocalisation"  onPress={()=>this.onPressLearnMore()}/>
      			</View>

			)
	}
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  mapStyle: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
});
export default Localisation